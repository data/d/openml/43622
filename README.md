# OpenML dataset: Binary-Dataset-of-Phishing-and-Legitimate-URLs

https://www.openml.org/d/43622

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description
The data set is provided csv file which provides the following resources that can be used as inputs for model building :
A collection of website URLs for 11001 websites. Each sample has 15 website parameters and a class label identifying it as a phishing website or not (0 or 1).
If URLs is Phished then label is 0 and for legitimate label is 1
The data set also serves as an input for project scoping and tries to specify the functional and non-functional requirements for it.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43622) of an [OpenML dataset](https://www.openml.org/d/43622). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43622/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43622/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43622/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

